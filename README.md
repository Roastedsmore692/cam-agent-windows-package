
Purpose - 
This repo provides the scripts and files necessary to install & distribute the CAM WMI agent via a Windows Pacakage. The .nupkg and it's supporting files in this repo
will install the CAM agent on any computer the script is executed on when installed via a Configuration Manager/Package manager. This allows for large-scale distributions of the agent via 
Configuration Manager.


Notes - 
This install script is utilizing a WMI install file on the local-machine. When distributing this package via a configuration manager, you will need to place the CAM agent WMI installer
on a network share that is accessible by all the machines that you wish to install the agent on. Once placed on the network share, change the chocolateyinstall.ps1 script to point to the installer 
in the network share.

Gotcha's - 
***This install script is for 9.3.2 versions. If you would like to use a newer version, make sure you download the right CAM WMI installer from
cherwell support and point your chocolatey script to it once downloaded.***

***Note the main point of this installation is to provide your access point to which the agent will report back to, to provide configuration item data. The CAM access point is obtained
from the CAM Administrator and is configured during set-up. To change the access point that is applied to the agent during the script/package installation, open the "Chocolateyinstall.ps1" script
and head down to line 43. in the "silentArgs" statement, you should see an existing ESMWEBSERVICE= statement, simply change the following URL to your CAM generated access point.
This will ensure the agent is installed with your proper CAM access point. 



